xjn.sh
======

## about

this site serves as a collection of guides and scripts covering a number of
use cases, including:

 - provisioning
 - configuration
 - automation

## usage

#### guides

more detailed instructions - select one from the navigation on the left

#### scripts

scripts are accessible in the form `xjn.sh/<os>/<script>` to make them easier to
type and easier to remember

scripts are usually referenced in guides, but some are also standalone - to see a full list of scripts, take a look at the [repository for this site](https://gitlab.com/xeijin-dev/up).
