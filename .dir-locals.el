;;; Directory Local Variables
;;; For more information see (info "(emacs) Directory Variables")

((nil
  (projectile-project-name . "up")
  (project-bg-colour . "#9b0e00")
  (project-fg-colour . "#c0c5ce")
  (project-anon-face . (:foreground "#c0c5ce" :background "#9b0e00"))
  (propertized-project-name . (#("up" 0 2 (face (:foreground "#c0c5ce" :background "#9b0e00")))))
  (project-main-org-file . "up.org")))
