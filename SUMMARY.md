# Summary

* [xjn.sh](README.md)
  * **epaper display**
    * [raspberry pi: wifi epaper display](./guides/epaper-display/waveshare-1248in-epaper-rpi.md)
    * [esp32: wifi epaper display](./guides/epaper-display/waveshare-1248in-epaper-esp32.md)
