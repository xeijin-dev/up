let
  sshKeys = builtins.getEnv "NIX_KEYS";
in
  import ./make-kexec.nix {
    extraConfig = {pkgs, ...}: {
      environment.systemPackages = [
        gnumake
      ];
      services.openssh = {
        enable = true;
        startWhenNeeded = true;
      };
      users.extraUsers.root.openssh.authorizedKeys.keys = [ "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIGtoJP5ks0Q0OWcngA3RSCTBLxMK1FXGBppSoOuXickv htdar (gitlab.com)" ];
      networking = {
        # firewall.allowedTCPPorts = [ 22 ];
        # usePredictableInterfaceNames = false;
        useDHCP = true;
      };
      # systemd.network.enable = true;
      # environment.etc."systemd/network/eth0.network".text = ''
      #   [Match]
      #   Name = eth0
      #   [Network]
      #   Address = 64.137.201.46/24
      #   Gateway = 64.137.201.1
      # '';
    };
  }
