esp32: wifi-connected waveshare 12.48in epaper display
===============================================

**note:** this guide is currently a WIP

----

This guide outlines the steps required to turn a [waveshare 12.48in e-paper module (B) {three colour version}](https://www.waveshare.com/wiki/12.48inch_e-Paper_Module_(B)) into a wifi-connected e-paper display with an ESP32 microcontroller


## advantages

compared to the raspberry pi version, the ESP32 ...

- consumes much less power (making a battery-powered version possible)
- starts up quickly (few seconds)
- is cheaper (ESP32 boards can be had for less than £10)

## pre-requisites

you'll need:

- an ESP32 microcontroller, make sure it has
  - the `WROVER` chip revision, **with at least 4MB PSRAM** 
  - *recommended:* get a board with a pinout which matches the waveshare epaper module PCB (see below) otherwise you may need to re-cable/solder
  - *optional:* board with a battery connector + battery (the T-Energy board linked above 
    - I used: [ICQUANZX ESP32 8MByte PSRAM WiFi & Bluetooth Module 18650 Battery ESP32-WROVER-B Development Board](https://www.amazon.co.uk/dp/B07VQJ1RH2/ref=cm_sw_em_r_mt_dp_ORQWFbDXM7P1C) 
      - this is a re-badged [LILYGO TTGO T-18/T-Energy](https://github.com/LilyGO/LILYGO-T-Energy/)
      - it comes with an 18650 Li-ion battery socket pre-soldered, though downside is the placement prevents direct/easy connection to the waveshare PCB (see below)
- waveshare 12.48in e-paper module (B), 3 colour display
- VSCode + Espressif extension (or just the usual Espressif ESP-IDF if you prefer)
  - ESP-IDF v4.2 is what I used (designated pre-release at time of writing, and not supported by platformIO unfortunately)
- a https://cale.es account, or your own URL where you can serve a BMP or JPG image in the display's native 1304x984 resolution

## hardware prep

unfortunately the waveshare board breaks the Espressif specifications - in particular, it makes use of pins 16 and 17 (which are reserved for PSRAM usage on boards that support it). The 12.48in epaper requires a large (in esp32 RAM terms) buffer of 160kb to drive it, which means we'll need to access the PSRAM if we want to do anything beyond a simple demo (e.g. loading the wifi library to grab a remote image). 

We'll fix this by physically re-wiring a couple of pins on the waveshare board to unused GPIO counterparts, then making an update to the configuration file. 

Here was the approach I took whilst developing/prototyping, though note the caveats below.

- 2 pin dupont jumper wire to remap `16 -> 26` and `17 -> 27` on the waveshare board(I picked these mainly for ease of recollection/tidiness)
- pin`26` was already used in `sdkconfig` so I changed the single existing reference to an unused GPIO pin (`21` in my case)
- either comb away or remove pins `16` and `17` from the *esp32 board* - these should not connect to the waveshare board at all (headers were not soldered on my board so I simply skipped these pins)

<img src="./img/jumper-layout.jpg" width="100"/> <img src="./img/jumper-wires-final.jpg" width="100"/>

(all credit to [Martin Fasani/CalEPD](https://github.com/martinberlin/cale-idf/wiki/Model-wave12i48.h#esp32-wroover-hack-for-this-epaper) for thinking up the original hack)

**downsides:** I had some issues with loose jumper wires (probably made worse by frequent connect/disconnecting) which introduced alot of frustration into initial development - I created `gpio_pin_check_upython.py` to help this, can be used to check this (requires flashing micropython in order to do so) - need to figure out how to do this in C++.

## configure & flash the firmware

`ESP-IDF` is a set of development tools for creating and managing firmware on Espressif ESP32 boards. Martin Fasani and the folks at https://cale.es created a custom firmware [cale-idf](https://github.com/martinberlin/cale-idf) that you can install on your ESP32 board. The firmware essentially (1) connects to your WiFi network (using the provided details) (2) downloads an image representing a dashboard from cale.es (or the specified URL serving a `BMP` or `JPG`) (3) goes to sleep for the specified interval, then repeats.

1. clone the repository: https://github.com/martinberlin/cale-idf (or alternatively, https://github.com/xeijin/cale-idf - and skip step 3)
2. edit the `sdkconfig` manually, or use the `menuconfig` / graphical menu - you want review (1) the CALE configuration (add your WiFi details, set sleep/refresh interval and provide an image URL and optionally your cale.es auth token) (2) the pin config in the `Display Configuration` section - it should align with the pinout for your board (and if applicable any hacks made in the hardware prep section earlier)
3. uncomment demos etc but leave `SRCS cale.cpp` in `main/CMakeLists.txt`
4. Execute 'Build, Flash & Monitor' from the ESP-IDF command palette in VSCode (`CMD/CTRL + SHIFT + P`)
5. Once the flash completes your ESP32 will pull down the image and should refresh according to your chosen interval

## todo

- test battery version & implement a battery low warning (notification and visual indicator)
- 3 color image processing

## resources

- [issue with 12.48in waveshare 3-colour #22](https://github.com/martinberlin/cale-idf/issues/22)
- [NFC or QR code to open interactive version on phone](https://www.notion.so/NFC-or-QR-code-to-open-interactive-version-on-phone-481aee6b585c41f0a038517202d306fb)
- [Sprites mods - Wifi E-ink display - Introduction](https://www.notion.so/Sprites-mods-Wifi-E-ink-display-Introduction-39dfbcc719ad4e0f966e9777586cc791)
- [waveshare 12.48" e-paper: size comparison](https://www.notion.so/waveshare-12-48-e-paper-size-comparison-92cc6ae4a5e14d4797ae68d8af2a0d94)
- [raspberry pi or esp32 to drive display](https://www.notion.so/raspberry-pi-or-esp32-to-drive-display-51050558a1214aae8a476fb348259575)
- [Raspberry Pi E Ink Task Organizer - YouTube](https://www.notion.so/Raspberry-Pi-E-Ink-Task-Organizer-YouTube-f43ddef78926431bb345d55a3f0ed301)
- [E-Ink Family Calendar Using ESP32](https://www.notion.so/E-Ink-Family-Calendar-Using-ESP32-a49f34bed63940ed9444e099b25e69dd)
- [ttgo t-energy schematic](https://www.notion.so/ttgo-t-energy-schematic-f6bdf94738754ab08b68ec32eaf0eff0)
- [Mountain Clock | Hackaday.io](https://www.notion.so/Mountain-Clock-Hackaday-io-dcc66b4c812f460a8239f69595749a09)
- [demo of 12.48in epaper using espressif esp32 w/ psram & cale.es firmware](https://www.notion.so/demo-of-12-48in-epaper-using-espressif-esp32-w-psram-cale-es-firmware-2e97ad74b78f4411b2d61f8291178ea9)
- [Model wave12i48.h · martinberlin/cale-idf Wiki · GitHub](https://www.notion.so/Model-wave12i48-h-martinberlin-cale-idf-Wiki-GitHub-dc2740b7237641699e2531672c17640f)
- [12.48inch e-Paper Module (B) - Waveshare Wiki](https://www.notion.so/12-48inch-e-Paper-Module-B-Waveshare-Wiki-4e63e74cee624f999f4bbda55eea7989)
- [martinberlin/cale-idf: ESP32 firmware that can abstract 12.48in waveshare display complexity](https://www.notion.so/martinberlin-cale-idf-ESP32-firmware-that-can-abstract-12-48in-waveshare-display-complexity-2249290d32e947c199f4c60fd4ac52b0)
- [comms with  ESP32 using node red (e.g. MQTT)](https://www.notion.so/comms-with-ESP32-using-node-red-e-g-MQTT-b2f5317147f34cad843424a5ee459a62)
- [RgbQuant.js - can reduce number of colours in image](https://www.notion.so/RgbQuant-js-can-reduce-number-of-colours-in-image-1c04fe1e328d473eb63eadaf1db284be)
- [customised gpio mapping for waveshare 12.48in & better cable mgmt](https://www.notion.so/customised-gpio-mapping-for-waveshare-12-48in-better-cable-mgmt-2858e94e123248d58b076a99cdc5f1fd)
- [ttgo t-energy pinout](https://www.notion.so/ttgo-t-energy-pinout-bef613f869994b5aa69a2421fba0a571)
