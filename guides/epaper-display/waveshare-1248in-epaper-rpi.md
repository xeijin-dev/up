raspberry pi: wifi-connected waveshare 12.48in epaper display
===============================================

This guide outlines the steps required to turn a [waveshare 12.48in e-paper module (B) {three colour version}](https://www.waveshare.com/wiki/12.48inch_e-Paper_Module_(B)) into a wifi-connected e-paper display with a raspbery pi 3B+


## pre-requisites

you'll need:

- a raspberry pi 3b+ (other models untested)
- waveshare 12.48in display
- an sd card and a macOS / linux / windows machine

## install steps

1. download the raspberry pi imaging tool from www.raspberrypi.org
2. install it and insert your SD card (format it using the tool if it's not already empty)
3. for the image choose the raspbian 'lite' install, select your SD card then hit 'write'
4. once complete you may need to re-insert your SD card (if it was ejected)
5. add a `wpa_supplicant.conf` file to the `boot` folder (replace with your wireless AP details)

```conf
ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
update_config=1
country=GB

network={
 ssid="<Name of your wireless LAN>"
 psk="<Password for your wireless LAN>"
}
```

6. add a blank file called `ssh` to the boot folder
7. connect the raspberry pi to the gpio pins on the waveshare module board
8. insert the sd card into the raspberry pi and connect the power
9. run `curl -sL xjn.sh/epd/rpi | sudo sh` and let installation complete
10. navigate to http://<rpi ip>:1880, then burger menu > import and copy-paste the contents of https://xjn.sh/nr/epd-webshot.json
