emacs, no x11, no sudo
======================

install terminal emacs when sudo is unavailable (as used on Theia/Gitpod/code-server)

```sh
curl -sL https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
chmod +x Miniconda3-latest-Linux-x86_64.sh
bash Miniconda3-latest-Linux-x86_64.sh
# accept the defaults and complete installation
source ~/.bashrc
conda install -c conda-forge emacs
mkdir ~/.config/
git clone https://github.com/hlissner/doom-emacs ~/.config/emacs 
git clone https://gitlab.com/xeijin-dev/doom-config ~/.config/doom
~/.config/emacs/bin/doom -y install
# optionally install tmux for detaching
conda install -c conda-forge tmux
# start emacs (-nw = terminal emacs)
emacs -nw <file>
# optionally add the following to .bashrc
export EDITOR="emacs -nw"
```
